from django.apps import AppConfig


class ShoesCoreConfig(AppConfig):
    name = 'shoes_core'
