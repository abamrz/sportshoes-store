from django.db import models

# Create your models here.

class Color(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=50)


class Brand(models.Model):
    code = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    country = models.CharField(max_length=100)


class ProductType(models.Model):
    name = models.CharField(max_length=100)
    gender = models.CharField(max_length=20, verbose_name='male/female')


class Product(models.Model):
    name = models.CharField(max_length=100)
    price = models.DecimalField(decimal_places=2, max_digits=4)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE, null=True)
    on_stock = models.BooleanField(default=False)
    on_sale = models.BooleanField(default=False)


class Detail(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    color = models.ForeignKey(Color, on_delete=models.CASCADE, null=True)
    size = models.CharField(max_length=100)
    qty = models.IntegerField(verbose_name='Quantity')
    type = models.ForeignKey(ProductType, on_delete=models.CASCADE, verbose_name=('type'))


class PaymentType(models.Model):
    name = models.CharField(max_length=50)
    comment = models.CharField(max_length=100)


class Payment(models.Model):
    allowed = models.BooleanField(default=False)
    type = models.ForeignKey(PaymentType, on_delete=models.CASCADE)
    date = models.DateTimeField(verbose_name='Payment date')
    paid = models.BooleanField(default=False)





