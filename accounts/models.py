from django.contrib.auth.models import User
from django.db import models
from shoes_core import models as core


class Status(models.Model):
    title = models.CharField(max_length=50)
    title_slug = models.CharField(max_length=50)


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    number = models.CharField(max_length=50)
    order_date = models.DateTimeField(verbose_name='Order date')
    ship_date = models.DateTimeField(verbose_name='Ship date')
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    payment = models.ForeignKey(core.Payment, on_delete=models.CASCADE, related_name="payment for order+")
    product = models.ForeignKey(core.Product, on_delete=models.CASCADE, related_name="order made for product+")


class Review(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(core.Product, on_delete=models.CASCADE)
    mark = models.DecimalField(decimal_places=1, verbose_name='Mark for product', max_digits=2)
    text = models.TextField(max_length=100, verbose_name='Text review')


class Profile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class CustomerUser(models.Model):
    profile = models.OneToOneField(Profile, on_delete=models.CASCADE)


class RetailerUser(models.Model):
    profile = models.OneToOneField(Profile, on_delete=models.CASCADE)


